// Create final store using all reducers and applying middleware
import { createBrowserHistory } from 'history';
// Redux utility functions
import { compose, createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware, connectRouter } from 'connected-react-router';
// Import all reducers
import stateReducer from '../reducers/index';
import {routerReducer} from 'react-router-redux'

// Configure reducer to store state at state.router
// You can store it elsewhere by specifying a custom `routerStateSelector`
// in the store enhancer below
export const history = createBrowserHistory();
const reducer = combineReducers({ reducer: stateReducer,routing:routerReducer});

const store = compose(
    // Enables your middleware:
    applyMiddleware(thunkMiddleware), // any Redux middleware, e.g. redux-thunk
    applyMiddleware(routerMiddleware(history)),
    // Provides support for DevTools via Chrome extension
    window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)(connectRouter(history)(reducer));


export default store;
