import React from 'react';
import {Layout} from "antd";
import {Router, Route, Redirect, Switch} from 'react-router-dom';
import App from "../containers/app/App";
import history from './history'


const { Header, Footer, Sider, Content } = Layout;
class PrivateRoute extends React.Component{
    render(){
        let { component: Component, ...rest } = this.props;
        return (<Route
            {...rest}
            render={props => {
                console.log(props);
                return localStorage.getItem("token")? (
                    <Component {...props} {...rest}/>
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: props.location}
                        }}
                    />
                )
            }

            }
        />)
    }
};

const router = (
    <Router history={history}>
        <Switch>
            <Route path="/" component={App}/>

        </Switch>
    </Router>
);



export default router;
