import * as fetchData from './fetchData'

/**
 *
 * @param fullpath
 * @returns {string} /user
 */
export const getFirstPath = function(fullpath){
    // 去掉第一个/
    // let fullpath = "";
    console.log("indexOf:",fullpath.indexOf("/"));
    fullpath = fullpath.substring(fullpath.indexOf("/")+1,fullpath.length);
    if(fullpath.indexOf("/") > -1){
        // 还有第二个/
        fullpath = fullpath.substr(0, fullpath.indexOf("/"));
    }
    return "/"+fullpath;
}

export const getData = (apiUrl, param, fetchStarted, fetchSuccess, fetchFailure) => {

    if(fetchStarted) fetchStarted();
    fetchData.getData(apiUrl, param).then(({ data: responseJson }) => {
        // console.log(responseJson) // 返回值
        fetchSuccess(responseJson);
    }).catch(function (error) {
        fetchFailure(error);
    })
};

export const postData = (url, param, fetchStarted, fetchSuccess, fetchFailure) => {

    if(fetchStarted) fetchStarted();
    setTimeout(
        ()=>{
            fetchData.postData(url, param).then(({data: responseJson }) => {
                console.log(responseJson);// 返回值
                if(fetchSuccess) fetchSuccess(responseJson);
            }).catch(function (error) {
                console.log("error:",error);
                if(fetchSuccess) fetchFailure(error);
            })
        }
    ,1000)
};
