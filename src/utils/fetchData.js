/**
 * Created by admin on 2016/10/10.
 */
import axios from 'axios'


//的post形式参数做了序列化的处理
export let instance = axios.create({
    timeout: 3000, //设置超时时间
    headers: {'content-type': 'application/x-www-form-urlencoded;charset=UTF-8'},
});
if(localStorage.getItem("token")){
    instance.defaults.headers.common['authorization'] = "Behind "+localStorage.getItem("token");
}

export const getData = (url, param ={}) => {
    return (
        instance.get(`${url}`, {
            params: param
        })
    )
}

export const postData = (url, param={}) => {
    let params = new URLSearchParams();
    for (let prop in param) {
        if (param.hasOwnProperty(prop)) {
            params.append(prop,param[prop])
        }
    }
    return (
        instance.post(`${url}`, params)
    )
}


// axios.defaults.baseURL = 'http://jhwz-api-test.mhw-design.com';
axios.defaults.baseURL = 'http://localhost:10111';

function xuliehua(oJson){
    for (var i in oJson) {
        if(Object.prototype.toString.call(oJson[i])=='[object Array]'){
            oJson[i]=JSON.stringify(oJson[i]);
        }
    }
    return oJson;
}
